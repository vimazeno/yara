## Yara detection

Ce repo permet de scripter en python l'outil de détection de malware [yara](https://yara.readthedocs.io/en/stable/gettingstarted.html) en utilisant les règles de détection de [reversinglabs-yara-rules](https://github.com/reversinglabs/reversinglabs-yara-rules).

Il est possible de faire un test avec [WannaCry](https://github.com/ytisf/theZoo/raw/master/malwares/Binaries/Ransomware.WannaCry) téléchargé à partir de [theZoo](https://github.com/ytisf/theZoo)

!! Attention ce repo installe des virus : à tester sur une vm offline uniquement

## Tester

créer et activer un environnement virtuel python

```
$ source bin/venv
```

compiler yara

```
$ bash bin/yara
```

télécharger les règles et un virus exemple 

```
$ bash bin/external
```

tester une règle unitaire

```
$ python unitary-rule.py
```

tester la règle wanacry sur l'exe infecté téléchargé de [theZoo](https://github.com/ytisf/theZoo)

```
$ python unitary-rule.py
```


## see also

* https://gist.github.com/kirantambe/f9e392eb6248b07eca9be1af4fda766e

* https://yara.readthedocs.io/en/stable/modules.html

* https://github.com/reversinglabs/reversinglabs-yara-rules

* https://github.com/ytisf/theZoo