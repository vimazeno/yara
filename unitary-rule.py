import yara
rule = yara.compile(source='rule foo: bar {strings: $a = "lmn" condition: $a}')
matches = rule.match(data='abcdefgjiklmnoprstuvwxyz')
print(matches)
print(matches[0].rule)
print(matches[0].tags)
print(matches[0].strings)