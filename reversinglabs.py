import yara
rules = yara.compile(filepaths={
  'Win32.Trojan.Emotet':'reversinglabs-yara-rules/yara/trojan/Win32.Trojan.Emotet.yara',
  'Win32.Ransomware.WannaCry':'reversinglabs-yara-rules/yara/ransomware/Win32.Ransomware.WannaCry.yara'
})

matches = rules.match('ed01ebfbc9eb5bbea545af4d01bf5f1071661840480439c6e5babe8e080e41aa.exe')
print(matches)
